import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np

# Example sizes for the spikes representing different metrics
sizes = [1000, 1500, 2000]  # Different heights for the spikes

# Base RGB color for the spikes
color = (1, 0, 0, 1)  # Solid red in RGBA

# Create a new figure and add a 3D subplot
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

# Create multiple cylinders (spikes)
for i, height in enumerate(sizes):
    base_radius = 0.1 * (i + 1)  # Increment the base radius for each spike

    # Create cylinder
    theta = np.linspace(0, 2 * np.pi, 30)  # Angle array
    z = np.linspace(0, height, 30)  # Height array
    theta_grid, z_grid = np.meshgrid(theta, z)  # Grids for angles and height
    x_grid = base_radius * np.cos(theta_grid)  # X coordinates
    y_grid = base_radius * np.sin(theta_grid)  # Y coordinates

    # Adjust the position for each cylinder so they don't overlap
    x_grid += i * 3 * base_radius  # Offset each cylinder along the x-axis

    # Plot the surface
    ax.plot_surface(x_grid, y_grid, z_grid, color=color)

# Set the aspect ratio to equal for x, y, z axes
ax.set_box_aspect([1, 1, 1])

# Hide the axes
ax.axis('off')

# Show the plot
plt.show()
