import http.server
import socketserver

PORT = 8000
DIRECTORY = "D:\Stuff\_Dev\stm-dashboard\prototypes\Spikes\InteractiveGlobe5.html"

class Handler(http.server.SimpleHTTPRequestHandler):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, directory=DIRECTORY, **kwargs)

if __name__ == "__main__":
    with socketserver.TCPServer(("", PORT), Handler) as httpd:
        print(f"Serving at port {PORT}")
        httpd.serve_forever()
